# Angular + Nodejs + Socket.io Chat

Simple chat application built with <b>Angular 6</b> and <b>NodeJs</b> using <b>socket.io</b> api.

## Features:
<ul>
  <li>Emojis</li>
  <li>Group chat</li>
  <li>Private chat (one to one)</li>
  <li>Profile Image from URL</li>
  <li>Send Images</li>
  <li>Send any file type</li>
  <li>Realtime user status update (connected or disconnected)</li>
  <li>Beautiful user interface using <b>ng-material</b></li>
</ul>

## Commands to keep structure:

### Create Angular 6 Project

`ng new {projectName} --style=scss --routing`

### Create Module

`ng g m {moduleName}`

### Create Component

`ng g c {moduleName}/components/{componentName} --module {moduleName}`

### Create Interface

`ng g i {interfaceName}/interfaces/{interfaceName}`

### Create Service

`ng g s {moduleName}/services/{serviceName}`

### Create Directive

`ng g d {moduleName}/directives/{directiveName}`

### Create Guard

`ng g g {moduleName}/guards/{guardName}`

By Convention we will add Guards to the `Auth` module.

### Start Development Server

`ng serve --watch`

### Build for production

`ng build --prod --build-optimizer --output-path public --base-href https://searleser97.gitlab.io/angular-nodejs-chat/public`
