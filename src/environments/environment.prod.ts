export const environment = {
  production: true
};

export const WS_URL = 'http://170.84.210.70:3000';
export const APP_NAME = 'Chat App';
export const USER_PASS = 'pass';
export const CHAT_TYPES = {
  oneToOne: 'one_to_one',
  publicGroup: 'public_group',
  privateGroup: 'private_group'
};
export const MESSAGE_TYPES = {
  TEXT: 'txt',
  FILE: 'file',
  IMAGE: 'img'
};
