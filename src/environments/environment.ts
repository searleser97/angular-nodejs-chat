// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false
};

export const WS_URL = 'http://localhost:3000';
export const APP_NAME = 'Chat App';
export const USER_PASS = 'pass';
export const CHAT_TYPES = {
  oneToOne: 'one_to_one',
  publicGroup: 'public_group',
  privateGroup: 'private_group'
};
export const MESSAGE_TYPES = {
  TEXT: 'txt',
  FILE: 'file',
  IMAGE: 'img'
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
