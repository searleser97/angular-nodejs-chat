import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {
  MatButtonModule,
  MatCheckboxModule,
  MatInputModule,
  MatFormFieldModule,
  MatCardModule,
  MatDividerModule,
  MatListModule,
  MatIconModule,
  MatChipsModule,
  MatButtonToggleModule,
  MatTooltipModule,
  MatExpansionModule
} from '@angular/material';

const MatModules = [
  CommonModule,
  MatButtonModule,
  MatCheckboxModule,
  MatInputModule,
  MatFormFieldModule,
  MatCardModule,
  MatDividerModule,
  MatListModule,
  MatIconModule,
  MatChipsModule,
  MatButtonToggleModule,
  MatTooltipModule,
  MatExpansionModule
];

@NgModule({
  imports: MatModules,
  exports: MatModules,
  declarations: []
})
export class MaterialModule { }
