import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {AuthRoutingModule} from './auth-routing.module';
import {LoginComponent} from './components/login/login.component';


import { AutofocusDirective } from '../shared/directives/autofocus.directive';
import {MaterialModule} from '../shared/modules/material.module';
import {FormsModule} from '@angular/forms';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';

@NgModule({
  imports: [
    CommonModule,
    AuthRoutingModule,
    BrowserAnimationsModule,
    MaterialModule,
    FormsModule
  ],
  declarations: [
    LoginComponent,
    AutofocusDirective
  ]
})
export class AuthModule {
}
