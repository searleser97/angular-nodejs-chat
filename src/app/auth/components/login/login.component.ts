import { Component, OnInit } from '@angular/core';
import { UserService } from '../../../shared/services/user.service';
import {APP_NAME, USER_PASS} from '../../../../environments/environment';
import { Router } from '@angular/router';
import { AuthService } from '../../services/auth.service';
import { ChatService } from '../../../chat/services/chat.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  user_id: string;
  app_name: string;
  profile_img: string;

  constructor(
    private authService: AuthService,
    private userService: UserService,
    private router: Router,
    private chatService: ChatService
  ) { }

  ngOnInit() {
    this.app_name = APP_NAME;
  }

  submit() {
    if (this.user_id === '') {
      alert('Enter a valid username');
      return;
    }
    const aux = this.user_id.split(';');
    this.user_id = aux[0];
    let ws_url = aux[1];
    if (ws_url === undefined || ws_url === '') {
      // ws_url = 'localhost:3000';
      ws_url = 'https://polar-crag-79323.herokuapp.com';
    }

    this.userService.setUserData(this.user_id, this.profile_img);
    if (!this.authService.authenticate()) {
      alert('usuario invalido');
      return;
    }
    console.log('ws_url: ' + ws_url);
    this.chatService.connect(USER_PASS, ws_url);
    this.router.navigate(['/chat']);
  }

}
