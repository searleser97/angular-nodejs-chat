export interface IncomingMessage {
  sender_id: string;
  sender_profile_img: string;
  type: string;
  content: object;
}
