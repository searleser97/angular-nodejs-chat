export interface OutgoingMessage {
  type: string;
  content: string;
}
