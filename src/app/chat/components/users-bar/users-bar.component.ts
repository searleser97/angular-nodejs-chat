import {Component, EventEmitter, OnDestroy, OnInit, Output} from '@angular/core';
import {ChatService} from '../../services/chat.service';
import {User} from 'src/app/shared/interfaces/user';
import {Room} from '../../interfaces/room';
import {UserService} from 'src/app/shared/services/user.service';

@Component({
  selector: 'app-users-bar',
  templateUrl: './users-bar.component.html',
  styleUrls: ['./users-bar.component.scss']
})
export class UsersBarComponent implements OnInit, OnDestroy {

  @Output() change = new EventEmitter<boolean>();

  users: User[];
  groups: Room[];
  listeners = [];
  group_name: string;
  user_id: string;

  constructor(private chatService: ChatService, private userService: UserService) {
  }

  ngOnInit() {
    this.user_id = this.userService.id;
    this.initListeners();
    this.getUsers();
    this.getGroups();
  }

  initListeners() {
    this.listeners.push(this.chatService.listenFor('users').subscribe(data => {
      this.users = data as User[];
    }));
    this.listeners.push(this.chatService.listenFor('groups').subscribe(data => {
      this.groups = data as Room[];
    }));
  }

  getUsers() {
    this.chatService.command('send-users');
  }

  startPrivateChatWith(user_id: string) {
    this.chatService.joinOneToOneRoomWith(user_id);
  }

  getGroups() {
    this.chatService.command('send-groups');
  }

  createGroupRoom() {
    this.chatService.createRoom(this.group_name);
  }

  startGroupChatIn(room_id: string) {
    this.chatService.joinGroupRoom(room_id);
  }

  conversationChange() {
    this.change.emit(true);
  }

  ngOnDestroy() {
    this.chatService.unsubscribeListeners(this.listeners);
  }

}
