import {AfterViewInit, Component, ElementRef, OnDestroy, OnInit, QueryList, ViewChild, ViewChildren} from '@angular/core';
import {ChatService} from '../../services/chat.service';
import {UserService} from 'src/app/shared/services/user.service';
import {IncomingMessage} from '../../interfaces/incoming-message';
import {MESSAGE_TYPES} from 'src/environments/environment';

@Component({
  selector: 'app-message-area',
  templateUrl: './message-area.component.html',
  styleUrls: ['./message-area.component.scss']
})
export class MessageAreaComponent implements OnInit, OnDestroy, AfterViewInit {

  @ViewChild('scrollMe') private myScrollContainer: ElementRef;
  @ViewChildren('message_divs') private message_divs: QueryList<any>;
  @ViewChild('download_elem') private download_elem: ElementRef;

  tooltip_pos = 'above';
  messages: any = [];
  listeners = [];
  userId: string;
  default_profile_img = 'https://avatars.servers.getgo.com/2205256774854474505_medium.jpg';
  MESSAGE_TYPES = MESSAGE_TYPES;

  constructor(private chatService: ChatService, private userService: UserService) {
    this.userId = userService.id;
  }

  ngOnInit() {
    this.initListeners();
  }

  ngAfterViewInit() {
    this.message_divs.changes.subscribe(t => {
      this.scrollToBottom();
    });
  }

  initListeners() {
    this.listeners.push(this.chatService.listenFor('msg').subscribe(msg => {
      this.messages.push(msg as IncomingMessage);
    }));
    this.listeners.push(this.chatService.listenFor('file').subscribe((data: any) => {
      this.download_elem.nativeElement.download = data.file_name;
      this.download_elem.nativeElement.href = 'data:application/octet-stream;base64,' + data.file;
      this.download_elem.nativeElement.click();
    }));
  }

  ngOnDestroy() {
    this.chatService.unsubscribeListeners(this.listeners);
  }

  getImage(img) {
    if (img === undefined) {
      return this.default_profile_img;
    } else {
      return img;
    }
  }

  scrollToBottom(): void {
    try {
      this.myScrollContainer.nativeElement.scrollTop = this.myScrollContainer.nativeElement.scrollHeight;
    } catch (err) {
    }
  }

  requestFile(file_name) {
    this.chatService.requestFile(file_name);
  }

  clearMessages() {
    this.messages = [];
  }

  getUrl(msg): string {
    console.log(msg.content.split('.')[1]);
    return 'data:image/' + msg.content.split('.')[1] + ';base64,' + msg.img;
  }

}
