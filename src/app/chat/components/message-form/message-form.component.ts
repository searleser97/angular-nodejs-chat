import {AfterViewInit, Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {ChatService} from '../../services/chat.service';
import {MESSAGE_TYPES} from 'src/environments/environment';
import {OutgoingMessage} from '../../interfaces/outgoing-message';

declare var SocketIOFileUpload;

@Component({
  selector: 'app-message-form',
  templateUrl: './message-form.component.html',
  styleUrls: ['./message-form.component.scss']
})
export class MessageFormComponent implements OnInit, AfterViewInit {

  @ViewChild('inputFile') private inputFile: ElementRef;
  message = '';

  constructor(private chatService: ChatService) {
  }

  ngOnInit() {
  }

  ngAfterViewInit() {
    const uploader = new SocketIOFileUpload(this.chatService.socket);
    uploader.listenOnInput(this.inputFile.nativeElement);
    uploader.addEventListener('progress', function (event) {
      const percent = event.bytesLoaded / event.file.size * 100;
      console.log('File is', percent.toFixed(2), 'percent loaded');
    });
  }

  sendMessage() {
    if (this.message.length === 0) {
      return;
    }
    console.log('message: ' + this.message);
    this.chatService.sendMessage({type: MESSAGE_TYPES.TEXT, content: this.message} as OutgoingMessage);
    this.message = '';
  }

  addEmoticon(code) {
    this.message += code;
  }
}
