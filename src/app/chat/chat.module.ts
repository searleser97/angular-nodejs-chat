import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ChatRoutingModule } from './chat-routing.module';

import { ChatWindowComponent } from './components/chat-window/chat-window.component';
import { MessageFormComponent } from './components/message-form/message-form.component';
import { MessageAreaComponent } from './components/message-area/message-area.component';
import { EmoticonsComponent } from './components/emoticons/emoticons.component';
import { UsersBarComponent } from './components/users-bar/users-bar.component';
import {FormsModule} from '@angular/forms';
import {MaterialModule} from '../shared/modules/material.module';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';

@NgModule({
  imports: [
    CommonModule,
    ChatRoutingModule,
    BrowserAnimationsModule,
    FormsModule,
    MaterialModule,
  ],
  declarations: [
    ChatWindowComponent,
    MessageFormComponent,
    MessageAreaComponent,
    EmoticonsComponent,
    UsersBarComponent
  ]
})
export class ChatModule { }
