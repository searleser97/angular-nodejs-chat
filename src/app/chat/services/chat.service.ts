import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import * as io from 'socket.io-client';
import {UserService} from '../../shared/services/user.service';

@Injectable({
  providedIn: 'root'
})

export class ChatService {

  public socket: SocketIOClient.Socket;

  constructor(private userService: UserService) {
  }

  connect(password, ws_url) {
    this.socket = io(ws_url);
    this.socket.emit('sign-in', {id: this.userService.id, profile_img: this.userService.profile_img, password: password});
  }

  joinOneToOneRoomWith(userId) {
    this.socket.emit('join-one-to-one-room', {userId: userId});
  }

  joinGroupRoom(room: string) {
    this.socket.emit('join-group-room', {room: room});
  }

  leaveRoom(room: string) {
    this.socket.emit('leave-room', room);
  }

  sendMessage(msg) {
    this.socket.emit('send-msg', msg);
  }

  command(resource: string) {
    this.socket.emit(resource);
  }

  listenFor(resource: string) {
    const observable = new Observable(observer => {

      this.socket.on(resource, (data) => {
        observer.next(data);
      });
      return () => {
        this.socket.disconnect();
      };
    });
    return observable;
  }

  unsubscribeListeners(listeners) {
    for (let i = 0; i < listeners.length; i++) {
      listeners[i].unsubscribe();
    }
  }

  createRoom(room_name) {
    this.socket.emit('create-group-room', {name: room_name});
  }

  requestFile(file_name) {
    this.socket.emit('send-file', {file_name: file_name});
  }
}
