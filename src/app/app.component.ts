import { Component, OnInit } from '@angular/core';
import { ChatService } from './chat/services/chat.service'

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'chatClient';

  constructor() { }

  ngOnInit() { }
}
